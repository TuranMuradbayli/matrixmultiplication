package matrixmultiplication;

public class MatrixMulti {

    public static void main(String[] args) {
        int[][] a = new int[2][3];
        int[][] b = new int[3][4];
        int[][] c = new int[2][4];

        ///////////////////////////// Fill array
        System.out.println("A");
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                a[i][j] = (int) (Math.random() * 10);
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
        //////////////////////
       System.out.println("B");
        for (int i = 0; i < b.length; i++) {
            for (int j = 0; j < b[i].length; j++) {
                b[i][j] = (int) (Math.random() * 10);
                System.out.print(b[i][j] + " ");
            }
            System.out.println();
        }
                
         
        /////////////////////////// Matrix multiplication
       int temp = 0;

       for (int r = 0; r < a.length; r++) // 2 times
        {
            for (int j = 0; j < b[0].length; j++) // 4 times
            {
                for (int i = 0; i < b.length; i++) // 3 times
                {
                    temp += a[r][i] * b[i][j];
                }
                c[r][j] = temp;
                temp = 0;
            }            
        }     
        /////////////////
        System.out.println("result");
        for (int i = 0; i < c.length; i++) {
            for (int j = 0; j < c[i].length; j++) {
                System.out.print(c[i][j] + " ");
            }
            System.out.println();
        }
    }
}
